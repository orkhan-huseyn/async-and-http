
const express = require('express');
const path = require('path');

const app = express();

app.use('/assets', express.static(path.resolve('assets')));

app.get('/', function(req, res) {
	res.sendFile(path.resolve('public', 'index.html'));
});

app.get('/text', function(req, res) {
	res.send('<p>Məni nəyə çağırırsan a?!</p>');
});

app.get('/json', function(req, res) {
	res.send({
		message: 'besdi de dayday'
	});
});

app.listen(8080, function () {
	console.log('Server is running on port 8080');
});