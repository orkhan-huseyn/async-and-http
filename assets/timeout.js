// async vs sync
let timerId;

button.addEventListener('click', function () {
	console.log('Before setTimeout!');
	timerId = setTimeout(logAfter5Seconds, 0);
	console.log('After setTimeout!');
	for (let i = 0; i < 1e10; i++) {}
	console.log('After hard work!');
});

// clearTimeout(timerId);

function logAfter5Seconds() {
	console.log('Log after 0 milliseconds.');
}

console.log('1');
console.log('2');
setTimeout(() => console.log('3'), 0);
console.log('4');