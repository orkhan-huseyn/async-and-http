
let requestId;

startButton.addEventListener('click', function() {
	function count() {
		counter.textContent++;
		requestId = requestAnimationFrame(count);
	}
	count();
});

stopButton.addEventListener('click', function() {
	cancelAnimationFrame(requestId);
});